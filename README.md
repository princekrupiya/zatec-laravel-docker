# Laravel and React and Docker

## How to run project

## 1. Build main containers


docker-compose up --build nginx -d


## 2. Install PHP packages


docker-compose run --rm composer install

docker-compose run --rm artisan key:generate

docker-compose run --rm artisan optimize


## 3. Install Node packages


docker-compose run --rm --service-ports npm install

docker-compose run --rm --service-ports npm run dev
